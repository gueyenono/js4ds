const original = "  This example uses text  "

const trim = (text) => {
  return text.trim()
}

const dot = (text) => {
  return text.replace(/ /g, ".")
}

const pipeline = (text, first, second) => {
  return second(first(text))
}

const trimThenDot = pipeline(original, trim, dot)

console.log(original)
console.log(`${trim(original)}`)
console.log(`${dot(original)}`)
console.log(`${pipeline(original, trim, dot)}`)
console.log(`${trimThenDot}`)


// Passing an array of functions

const pipeline2 = (initial, operations) => {
  
  let current = initial
  
  for(let op of operations){
    current = op(current)
  }
  
  return current
  
}

console.log(`The result of the pipeline2 function is: ${pipeline2(original, [trim, dot])}`)

const double = (text) => {
  return text + text
}

console.log(`You pass (${original}) to the double function, you get => ${double(original)}`)

console.log(`Applying all 3 operations on our text yields: ${pipeline2(original, [trim, dot, double])}`)