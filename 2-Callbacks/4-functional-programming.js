// create a test array
const test = [1, 2, 3]

// Define function
const impure = (values) => {
  for(let i in values){
    values[i] += 1
  }
  return values
}

// Run function
console.log(`Array before function is run: ${test}`)
impure(test)
console.log(`Array after function is run: ${test}`)

// Correct way of performing this task

const test2 = [1, 2, 3]

const pure = (values) => {
  
  newArray = []
  
  for(let v of values){
    newArray.push(v += 1)
  }
  
  return newArray
}

console.log(`Array before correct function is run: ${test2}`)
pure(test2)
console.log(`Array after correct function is run: ${test2}`)
console.log(`New array after correct function is run: ${pure(test2)}`)

// Array.some() and Array.every()
const data = ["this", "is", "a", "test"]
console.log(`Are some elements of the list longer than 3:`, data.some((x) => {return x.length >= 3}))
console.log(`Are all elements of the list longer than 3:`, data.every((x) => {return x.length >= 3}))

// Array.filter
console.log(`Here are the elements that have more than 3 characters:`, data.filter((x) => {return x.length >= 3}))

// Do all elements with at least 3 characters start with a "t"?
const threeCharsAtLeast = data.filter((x) => {return x.length >= 3})
console.log("Do all elements start with a t?:", threeCharsAtLeast.every((x) => {return x[0] === "t"}))

// Array.map
const sliced = data.map((x) => x.slice(0, 2))
console.log(`The first two characters of each element: ${sliced}`)

// Array.reduce
const makeText = (values) => {
  text = ""
  for(let v of values){
    text += v + " "
  }
  return text
}

const makeAcronym = (values) => {
  letter = ""
  for(let v of values){
    letter += v[0]
  }
  return letter
}

console.log(`The acronym of ${makeText(data).trim()} is ${makeAcronym(data)}.`)

const concatFirst = (accumulator, nextValue) => {
  return accumulator + nextValue[0]
}

let acronym = data.reduce(concatFirst, "")

console.log(`acronym of ${data} is ${acronym}`)

acronym2 = data.reduce((accum, next) => {
  return accum + next[0]
}, "")
console.log(`The acronym of ${data} is ${acronym2}`)