// Basic forEach example

// https://dmitripavlutin.com/foreach-iterate-array-javascript/

// 1- Basic forEach() example

const colors = ["blue", "green", "white"]

const iterate = (item) => {
  console.log(item)
}

colors.forEach(iterate)


// 2- Index of the iterated element

const iterate2 = (item, index) => {
  console.log(`${item} has index ${index}.`)
}

colors.forEach(iterate2)

// 3- Access the array inside the callback

const iterate3 = (item, index, array) => {
  
  console.log(item)
  
  if(index === array.length - 1){
    console.log("The last iteration.")
  }
}

colors.forEach(iterate3)


// Side Effects With foreEach

const vals = [1, 2, 3]

const doubleInPlace = (values) => {
  values.forEach((item, index, array) => { array[index] *= 2 })
}

console.log(vals)
doubleInPlace(vals)
console.log(vals)


// Annotating Data

data = [
  {'date': '1977-7-16', 'sex': 'M', 'species': 'NL'},
  {'date': '1977-7-16', 'sex': 'M', 'species': 'NL'},
  {'date': '1977-7-16', 'sex': 'F', 'species': 'DM'},
  {'date': '1977-7-16', 'sex': 'M', 'species': 'DM'},
  {'date': '1977-7-16', 'sex': 'M', 'species': 'DM'},
  {'date': '1977-7-16', 'sex': 'M', 'species': 'PF'},
  {'date': '1977-7-16', 'sex': 'F', 'species': 'PE'},
  {'date': '1977-7-16', 'sex': 'M', 'species': 'DM'}
]

newData = [
  {'seq': 3, 'year': '1977', 'sex': 'F', 'species': 'DM'},
  {'seq': 7, 'year': '1977', 'sex': 'F', 'species': 'PE'}
]

console.log(data)

const transformObject = (object) => {
  
  return object
    .filter((x) => x["sex"] === "F")
    .map((obj, i) => {
      return {
        seq: i + 1,
        year: obj["date"].slice(0, 4),
        sex: obj["sex"],
        species: obj["species"]
      }
    })
  
}

console.log(transformObject(data))