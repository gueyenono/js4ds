const transform = (values, operation) => {
  let result = []
  for(let v of values){
    result.push(operation(v))
  }
  return result
}

const data = ["one", "two", "three"]
const upper = transform(data, (x) => {return x.toUpperCase()})
console.log(`${data} => ${upper}`)

const firstLetter = transform(data, (x) => {return x[0].toUpperCase()})
console.log(`${data} => ${firstLetter}`)