class Square {
  
  constructor (size) {
    this.name = "square"
    this.size = size
  }
  
  area() {return this.size * this.size}
  perimeter() {return 4 * this.size}
  
}

class Circle {
  
  constructor (radius) {
    this.name = "circle"
    this.radius = radius
  }
  
  area() {return Math.PI * this.radius * this.radius}
  perimeter() {return 2 * Math.PI * this.radius}
  
}

class Rectangle {
  
  constructor (width, height) {
    this.name = "rectangle"
    this.width = width
    this.height = height
  }
  
  area() {return this.width * this.height}
  perimeter() {return 2 * this.width * this.height}
  
}

const everything = [
  new Square(3.5),
  new Circle(2.5),
  new Rectangle(1.5, 0.5)
]

for(let thing of everything){
  
  console.log(`Shape name: ${thing.name} \n Area: ${thing.area()} \n Perimeter: ${thing.perimeter()}. \n\n`)
  
}