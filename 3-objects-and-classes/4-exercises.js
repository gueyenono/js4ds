// Delays

class Delay {
  
  constructor (initial_value) {
    this.initial_value = initial_value
  }
  
  call (new_value) {
    let previous_value
    [previous_value, this.initial_value] = [this.initial_value, new_value]
    return previous_value
  }
  
}

const example = new Delay('a')
for (let value of ['b', 'c', 'd']) {
  console.log(value, '->', example.call(value))
}


// Filtering

class Filter {
  
  constructor (values) {
    this.values = values
  }
  
  call (input) {
    
    if(this.values.some((x) => {return x === input})){
      return input
    } else {
      return null
    }
    
  }
  
}

const example2 = new Filter(['a', 'e', 'i', 'o', 'u'])
for (let value of ['a', 'b', 'c', 'd', 'e']) {
  console.log(value, '->', example2.call(value))
}


// Active Expressions

console.log(`==============================================================`)

class Active {
  constructor (name, transform) {
    this.name = name
    this.transform = transform
    this.subscribers = []
  }

  subscribe (someone) {
    this.subscribers.push(someone)
  }

  update (input) {
    console.log(this.name, 'got', input)
    const output = this.transform(input)
    for (let s of this.subscribers) {
      s.update(output)
    }
  }
}

const start = new Active('start', (x) => Math.min(x, 10))
const left = new Active('left', (x) => 2 * x)
const right = new Active('right', (x) => x + 1)
const final = new Active('final', (x) => x)

start.subscribe(left)
start.subscribe(right)
left.subscribe(final)
right.subscribe(final)

start.update(123)


// 1. 
// a. The input 123 is passed to the update method of the start
// b. "start got 123"
// c. "left got 10"
// d. "final got 20"
// e. "right got 10"
// f. "final got 11"

// 2.

console.log('==============================================')

class Active2 {
  
  constructor (name, transform) {
    this.name = name
    this.transform = transform
    this.subscribers = []
  }
  
  subscribe (someone) {
    this.subscribers.push(someone)
  }
  
  update (input) {
    
    console.log(this.name, 'got', input)
      const output = this.transform(input)
      for (let s of this.subscribers) {
        s.update(output)
      }
  }
  
}

const start2 = new Active2('start', (x) => Math.min(x, 10))
const left2 = new Active2('left', (x) => 2 * x)
const right2 = new Active2('right', (x) => x + 1)
const final2 = new Active2('final', (x) => x)
const middle = new Active2("middle")

start2.subscribe(left)
start2.subscribe(right)
start2.subscribe(middle)
left2.subscribe(final)
right2.subscribe(final)

// start2.update(123)


class Delay2 {
  
  constructor (value) {
    this.value = value
  }
  
  transform (new_value) {
    console.log(this.value)
    this.value = new_value
  }
  
}

const d = new Delay2(5)
d.transform(6)
d.transform(7)