const limits = (values) => {
  
  if(!values.length){
    return [undefined, undefined]
  }
  
  let low = values[0]
  let high = values[0]
  
  for(let v of values){
    if(v < low) low = v
    if(v > high) high = v
  }
  
  return [low, high]

}

const values = [
  [2, 10, 1],
  [14, 14, 14],
  [0, -1, -58], 
  []
]

for(let value of values){
  console.log(`The limits of ${value} are ${limits(value)}.`)
}