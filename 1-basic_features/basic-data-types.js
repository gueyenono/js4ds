console.log("\n")

const aNumber = 123.45
console.log("The type of ", aNumber, "is", typeof aNumber, ".")

console.log("----")

const anInteger = 123
console.log("The type of", anInteger, "is", typeof anInteger, ".")

console.log("----")

const aString = "Some text"
console.log("The type of", aString, "is", typeof aString, ".")

console.log("----")

console.log("The type of", console.log, "is", typeof console.log, ".")

console.log("----")

const otherValues = [true, undefined, null]
for(let value of otherValues){
  console.log("The type of", value, "is", typeof value)
}


const allValues = [123.45, 123, "some text", console.log, true, undefined, null]
for(let v of allValues){
  console.log("The type of", v, "is", typeof v, "\n")
}