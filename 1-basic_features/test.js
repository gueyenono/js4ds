// Define function to append array

const append = (values, newElements) => {
  
  for(let v of newElements){
    
    if(typeof v === "number"){
      values.push(v)
    }
    
  }
  
  console.log(`The transformed array is now ${values}.`)
  
  return values
}

x = append([1, 2, 3], [4, 5, 6])
console.log(x)

const y = [1, 5, -50]
console.log(`y.reverse(${y}) -> ${y.reverse(y)}`)