// A- typeof

// 1. typeof is an operator and not a function.


// B- Fill in the blanks

// 1. Array.push() appends an array.

// 2. A while loop keeps running the code in its body until the condition is no longer satisfied.

// 3. += is an operator designed to update the value of a variable by adding to its current value.

// 4. Array.reverse() is a method that reverses the order of the elements of an array.

// 5.a Code in the exercise

let current = 0
let table = []

while(current <= 5){
  
  const entry = `The square of ${current} is equal to ${current * current}.`
  table.push(entry)
  current += 1
  
}

table.reverse()

for(let line of table){
  console.log(line)
}

// 5.b Wrap the code inside a function

const square = (number) => {
  
  let current = 0
  let table = []
  
  while(current <= number){
    const entry = `The square of ${current} is ${current * current}.`
    table.push(entry)
    current += 1
  }
  
  table.reverse()
  
  for(let line of table){
    console.log(line)
  }
  
}

console.log(square(10))


// C- What is truth?

const isTruthy = (element) => {
  
  if(!Array.isArray(element) || element.length === 0) return undefined
  
  let truthArray = []
  
  for(let el of element){
    
    if(el){
      truthArray.push("truthy")
    } else {
      truthArray.push("falsy")
    }
    
  }
  
  for(let i in element)
  console.log(`${element[i]} is ${truthArray[i]}.`)
  
}

isTruthy([0, 1, 2, "", "abc", true, false, [2, 5], undefined, null, console.log])


// D- The Shape of Things to Come

// 1. The advantage of returning [undefined, undefined], to me, is that the function is expected to return an array of 2 elements. So if a user sees that the output is [undefined, undefined], he/she will be more likely to realize that there is an issue with the input fed to the function.


// E- Combining Different Types

// 1. NaN means "Not a number"

// 2. As an R user, the results matched my expectation. The result of the second loop is a NaN due to the fact that one of the elements in the second array is a NaN.

const first = [3, 7, 8, 9, 1]
console.log(`aggregating ${first}.`)

let total = 0
for(let d of first){
  total += d
}

console.log(total)

const second = [0, 3, -1, NaN, 8]
console.log(`aggregating ${second}`)

let total2 = 0
for(let d of second){
  total2 += d
}

console.log(total2)


// F- What does this do?

const genus = 'Callithrix'
const species = 'Jacchus'
const creature = {genus, species}
console.log(creature)
console.log(JSON.stringify(creature))


// G- Destructuring Assignment


// Program 1
const creature = {
  genus: "Callithrix",
  species: "Jacchus"
}

const {genus, species} = creature
console.log(`genus is ${genus}.`)
console.log(`species is ${species}.`)

// Program 2
const creature = {
  first: 'Callithrix',
  second: 'Jacchus'
}
const {genus, species} = creature
console.log(`genus is ${genus}`)
console.log(`species is ${species}`)


// H- Return To Me, For My Heart Wants You Only

const verbose_sum = (first, second) => {
  
  console.log(`Going to add ${first} to ${second}`)
  let total = first + second
  return total
  console.log(`Finished summing`)
  
}

var result = verbose_sum(3, 6)
console.log(result)